# Grape Pinger
###### Pinger uses ruby version 2.7.2
###### To run the application you need:

- bundle
- rake db:create
- rake db:migrate
- puma


Start pinging the host:
- POST /api/v1/ip/address=8.8.8.8

Stop host ping:
- DELETE /api/v1/ip/address=8.8.8.8

View the host statistics for the period:
- GET /api/v1/ip/statistic?address=8.8.8.8&from_datetime=2021-01-01 01:01:01&to_datetime=2021-07-02 01:01:01

See which hosts are currently being pinged:

- GET /api/v1/ip/list

Swagger documentation link:

- http://localhost:9292/api/v1/ip/swagger_doc