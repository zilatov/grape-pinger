require 'pry'
require "rack/test"

describe do
  include Rack::Test::Methods

  def app
    App::Api::V1::Root
  end

  context 'GET /api/v1/ip/list' do
    it 'returns an empty array' do
      get '/api/v1/ip/list'
      expect(last_response.status).to eq(200)
      expect(JSON.parse(last_response.body)).to eq []
    end
  end
end
