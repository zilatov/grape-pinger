class AddIndexToIcmpRequests < ActiveRecord::Migration[6.1]
  def change
    add_index :icmp_requests, :time
  end
end
