class CreateIcmpRequests < ActiveRecord::Migration[6.1]
  def change
    create_table :icmp_requests do |t|
      t.boolean :success
      t.float :time
      
      t.references :ip, index: true, foreign_key: true
      t.timestamps null: false
    end    
  end
end
