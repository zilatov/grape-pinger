class CreateIps < ActiveRecord::Migration[6.1]
  def change
    create_table :ips do |t|
      t.string :address
      t.boolean :active, :default => false
    
      t.timestamps null: false
    end
  end
end
