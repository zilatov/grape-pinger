require_relative 'app/api/v1/root'
require 'otr-activerecord'

use OTR::ActiveRecord::ConnectionManagement
OTR::ActiveRecord.configure_from_file! 'config/database.yml'
OTR::ActiveRecord.establish_connection!


run Api::V1::Root
