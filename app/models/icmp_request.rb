# == Schema Information
#
# Table name: icmp_requests
#
#  id         :bigint           not null, primary key
#  success    :boolean
#  time       :float
#  ip_id      :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'otr-activerecord'

class IcmpRequest < ActiveRecord::Base
  belongs_to :ip

  scope :created_at_between, ->(from_datetime, to_datetime) {
    where(
      'created_at BETWEEN :from_datetime AND :to_datetime',
      from_datetime: from_datetime,
      to_datetime: to_datetime || DateTime.now
    )
  }

  scope :success, -> {
    where(success: true)
  }

  scope :failure, -> {
    where(success: false)
  }  
end
