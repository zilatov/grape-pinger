# == Schema Information
#
# Table name: ips
#
#  id         :bigint           not null, primary key
#  address    :string
#  active     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'otr-activerecord'

class Ip < ActiveRecord::Base
  has_many :icmp_requests

  scope :active, -> { where(active: true) }

  def active?
    active == true
  end

  def disable!
    update!(active: false)
  end  

  def enable!
    update!(active: true)
  end  
end
