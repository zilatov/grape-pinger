require 'grape'
require 'grape-swagger'
require 'dry-types'
require_relative 'helpers/list'
require_relative 'helpers/pinger'
require_relative 'helpers/statistic'
require_relative 'validations/ip_format'

module Api
  module V1
    class Root < Grape::API
      helpers Helpers::List

      get('/') do
        content_type 'text/plain'
        "I'm fine!"
      end

      namespace :api do
        namespace :v1 do
          namespace :ip do
            format :json

            params do
              requires :address, type: Validations::IpFormat
            end

            post do
              Helpers::Pinger.new(params[:address].value).call
            end  
            
            params do
              requires :address, type: Validations::IpFormat
            end

            delete do
              Ip.find_by(address: params[:address].value).disable!
            end

            params do
              requires :address, type: Validations::IpFormat
              requires :from_datetime, type: DateTime
              requires :to_datetime, type: DateTime
            end  

            get :statistic do
              Helpers::Statistic.new(declared(params)).fetch_json
            end  
            
            get :list do
              Helpers::List.pinged_addresses
            end

            add_swagger_documentation
          end  
        end  
      end
    end
  end
end
