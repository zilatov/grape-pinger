require './app/models/ip'
require './app/models/icmp_request'
require 'pry'

module Api
  module V1
    module Helpers
      class Statistic
        def initialize(params)
          @address = params[:address].value
          @from_datetime = params[:from_datetime]
          @to_datetime = params[:to_datetime]
        end  

        def fetch_json
          return { error: 'I do not know such an address or have not had time to make at least one request'} if requests.empty?
          {
            average: average,
            minimum: success_requests.minimum(:time),
            maximum: success_requests.maximum(:time),
            median:  median,
            stddev:  standard_deviation,
            loss:    percentage_of_losses(success_requests, failure_requests)
          }
        end

        private

        def requests
          @requests ||=
            Ip.find_by(address: @address)
              .icmp_requests
              .created_at_between(@from_datetime, @to_datetime)
        end

        def success_requests
          @success_requests ||= 
            requests.success
        end

        def failure_requests
          @failure_requests ||=
            requests.failure
        end

        def percentage_of_losses(success_requests, failure_requests)
          return unless success_requests.present? && failure_requests.present?
          ((failure_requests.size.to_f / success_requests.size.to_f) * 100).to_i
        end

        def standard_deviation
          relation = success_requests.select('stddev(time) as stdev_time')
          relation.take.stdev_time.round(3)
        end

        def average
           success_requests.average(:time).to_f.round(3)
        end

        def median
          relation = success_requests.select('percentile_disc(0.5) within group (order by time) as median')
          relation.take.median
        end
      end
    end  
  end
end
