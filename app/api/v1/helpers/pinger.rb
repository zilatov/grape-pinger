require './app/models/ip'
require './app/models/icmp_request'

module Api
  module V1
    module Helpers
      class Pinger
        def initialize(address)
          @address = address
        end

        def call
          ip = find_or_create_ip
          return { error: "I'm already pinging this address" } if ip.active?
          ip.enable!
          ping_address(ip)
        end

        private

        def find_or_create_ip
          Ip.find_or_create_by!(address: @address)
        end

        def ping_address(ip)
          IO.popen("ping #{ip.address}") do |io|
            while (line = io.gets) do
              unless ip.reload.active?
                Process.kill('TERM', io.pid)
              end

              if line.include?('time=')
                time = line.split('time=').last.split(' ').first.to_f
                data = { success: true, time: time, ip_id: ip.id }
              elsif line.include?('Request timeout')  
                data = { success: false, ip_id: ip.ip }
              end  
              IcmpRequest.create!(data)
            end
          end
        end
      end
    end
  end
end
