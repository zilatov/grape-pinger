require './app/models/ip'

module Api
  module V1
    module Helpers
      module List
        extend Grape::API::Helpers

        def self.pinged_addresses
          addresses = Ip.active.pluck(:address)

          { pinged_addresses: addresses }
        end  
      end
    end
  end
end
