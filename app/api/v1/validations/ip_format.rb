require 'dry-types'

module Validations
  class IpFormat
    attr_reader :value
    def initialize(address)
      @value = address
    end

    def self.parse(value)
      return new(value) if IPAddr.new(value).kind_of?(IPAddr)
    rescue
      Grape::Types::InvalidValue.new(':unsupported format. I know how to ping an v4 or v6')
    end
  end
end
